$('a[href*="#"]').on('click', function(e) {
    e.preventDefault();

    $('html, body').animate({
        scrollTop: $($(this).attr('href')).offset().top
    }, 800, 'linear');
});

$(document).ready(function() {
    const height = document.documentElement.clientHeight;
    const button = $('#button-up');
    $(window).scroll(function() {
        if ($(this).scrollTop() > height) {
            button.fadeIn();
        } else {
            button.fadeOut();
        }
    });
    button.on('click', function() {
        $('body, html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });
});

$('#button-toggle').click(function() {
    $('.animated').slideToggle(1000);
    $('.art-resort-spain').css('display', 'flex');
});